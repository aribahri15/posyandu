@extends('layouts.navigation')
@section('title', 'Posyandu')
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Posyandu
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Posyandu</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List Posyandu</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <button type="button" class="btn btn-primary" style="margin-bottom: 10px;" data-toggle="modal"
                            data-target="#modalAddPosyandu">Add Posyandu</button>
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Desa</th>
                                    <th>Kecamatan</th>
                                    <th>Kabupaten</th>
                                    <th>Ketua Posyandu</th>
                                    <th>No HP</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Burhan</td>
                                    <td>Banjarsari</td>
                                    <td>Ajibarang</td>
                                    <td>Banyumas</td>
                                    <td>Bambang</td>
                                    <td>0812345678910</td>
                                    <td>
                                        <button type="button" class="btn btn-xs btn-info" data-toggle="modal"
                                            data-target="#modalUpdatePosyandu">Edit
                                            Posyandu</button>
                                        {{-- <br>
                                        <button type="button" class="btn btn-xs btn-danger">Hapus
                                            Posyandu</button> --}}
                                    </td>
                                </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modalAddPosyandu" tabindex="-1" role="dialog" aria-labelledby="modalAddPosyanduLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Posyandu</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="province">Provinsi</label>
                        <input type="text" class="form-control" id="province" name="province"
                            placeholder="Enter province">
                    </div>
                    <div class="form-group">
                        <label for="city">Kabupaten / Kota</label>
                        <input type="text" class="form-control" id="city" name="city" placeholder="Enter city">
                    </div>
                    <div class="form-group">
                        <label for="district">Kecamatan</label>
                        <input type="text" class="form-control" id="district" name="district"
                            placeholder="Enter district">
                    </div>
                    <div class="form-group">
                        <label for="village">Desa</label>
                        <input type="text" class="form-control" id="village" name="village" placeholder="Enter village">
                    </div>
                    <div class="form-group">
                        <label for="chairmanName">Nama Ketua</label>
                        <input type="text" class="form-control" id="chairmanName" name="chairmanName"
                            placeholder="Enter chairman name">
                    </div>
                    <div class="form-group">
                        <label for="phone">No HP</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalUpdatePosyandu" tabindex="-1" role="dialog" aria-labelledby="modalUpdatePosyanduLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Update Posyandu</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="province">Provinsi</label>
                        <input type="text" class="form-control" id="province" name="province"
                            placeholder="Enter province">
                    </div>
                    <div class="form-group">
                        <label for="city">Kabupaten / Kota</label>
                        <input type="text" class="form-control" id="city" name="city" placeholder="Enter city">
                    </div>
                    <div class="form-group">
                        <label for="district">Kecamatan</label>
                        <input type="text" class="form-control" id="district" name="district"
                            placeholder="Enter district">
                    </div>
                    <div class="form-group">
                        <label for="village">Desa</label>
                        <input type="text" class="form-control" id="village" name="village" placeholder="Enter village">
                    </div>
                    <div class="form-group">
                        <label for="chairmanName">Nama Ketua</label>
                        <input type="text" class="form-control" id="chairmanName" name="chairmanName"
                            placeholder="Enter chairman name">
                    </div>
                    <div class="form-group">
                        <label for="phone">No HP</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('js')
<!-- DataTables -->
<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
      $('#userTable').DataTable({
      });
    });
</script>
@endsection