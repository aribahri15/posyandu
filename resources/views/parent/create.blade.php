@extends('layouts.navigation')
@section('title', 'Parent')
@section('css')
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Parent
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Parent</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List Parent</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- form start -->
                        <form role="form">
                            <div class="box-body">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="mothersName">Nama Lengkap Ibu</label>
                                        <input type="email" class="form-control" id="mothersName" name="mothersName"
                                            placeholder="Enter mother's name">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="province">Provinsi</label>
                                        <input type="email" class="form-control" id="province" id="province"
                                            placeholder="Enter province">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="birthPlace">Tempat Lahir</label>
                                        <input type="email" class="form-control" id="birthPlace" name="birthPlace"
                                            placeholder="Enter birth place">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="birthDate">Tanggal Lahir</label>
                                        <input type="email" class="form-control" id="birthDate" name="birthDate"
                                            placeholder="Enter birth date">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="city">Kabupaten / Kota</label>
                                        <input type="email" class="form-control" id="city" id="city"
                                            placeholder="Enter city">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="address">Alamat</label>
                                        <input type="email" class="form-control" id="address" name="address"
                                            placeholder="Enter address">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="districts">Kecamatan</label>
                                        <input type="email" class="form-control" id="districts" id="districts"
                                            placeholder="Enter districts">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="phone">No HP</label>
                                        <input type="email" class="form-control" id="phone" name="phone"
                                            placeholder="Enter phone">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="village">Desa</label>
                                        <input type="email" class="form-control" id="village" id="village"
                                            placeholder="Enter village">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="phone">Password</label>
                                        <input type="email" class="form-control" id="phone" name="phone"
                                            placeholder="Enter phone">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="posyandu">Posyandu</label>
                                        <input type="email" class="form-control" id="posyandu" id="posyandu"
                                            placeholder="Enter posyandu">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('js')
@endsection