@extends('layouts.navigation')
@section('title', 'Parent')
@section('css')
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Parent
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Parent</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-female"></i> Ibu Baim
                    <small class="pull-right">Date: 2/10/2014</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <strong>Alamat</strong>
                <address>
                    Ajibarang
                </address>
                <strong>Tempat, Tanggal Lahir</strong>
                <address>
                    Ajibarang, 1 Januari 2021
                </address>
                <strong>Nomor Telepon</strong>
                <address>
                    0812345678910
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <strong>Provinsi</strong>
                <address>
                    Jawa Tengah
                </address>
                <strong>Kabupaten / Kota</strong>
                <address>
                    Ajibarang
                </address>
                <strong>Kecamatan</strong>
                <address>
                    Ajibarang
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <strong>Desa</strong>
                <address>
                    Jawa Tengah
                </address>
                <strong>Posyandu</strong>
                <address>
                    Posyandu
                </address>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <button type="button" class="btn btn-info pull-right" style="margin-right: 5px;">
                    <i class="fa fa-pencil"></i> Edit
                </button>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List Child</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <a href="/child/create" class="btn btn-primary" style="margin-bottom: 10px;">Add Child</a>
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama Anak</th>
                                    <th>Jenis Kelamin</th>
                                    <th>TTL</th>
                                    <th>No KIA</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Baim</td>
                                    <td>Laki Laki</td>
                                    <td>Ajibarang, 1 Januari 2021</td>
                                    <td>12345678910</td>
                                    <td>
                                        <a class="btn btn-xs btn-info" href="/child/1">Detail</a>
                                        {{-- <br>
                                        <button type="button" class="btn btn-xs btn-danger">Hapus
                                            Parent</button> --}}
                                    </td>
                                </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
@section('js')
@endsection