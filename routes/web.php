<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('welcome');
});
Route::get('/user', function () {
    return view('user.index');
});
Route::get('/posyandu', function () {
    return view('posyandu.index');
});
Route::get('/parent', function () {
    return view('parent.index');
});
Route::get('/parent/1', function () {
    return view('parent.detail');
});
Route::get('/parent/create', function () {
    return view('parent.create');
});
Route::get('/child', function () {
    return view('child.index');
});
Route::get('/child/create', function () {
    return view('child.create');
});
Route::get('/child/1', function () {
    return view('child.detail');
});
Route::get('/setting', function () {
    return view('setting.index');
});
